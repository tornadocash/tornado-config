# Tornado Cash contracts info

Technical information about all Tornado Cash contracts, token distribution and ENS domains

### Requirements

Node 12 or higher. This package doesn't have any other build dependencies.

Install development dependencies:

```bash
npm i
```

### Build

```bash
npm run build
```

### Licence

MIT
