export type AvailableChainIds = 1 | 5 | 10 | 56 | 100 | 137 | 42161 | 43114;
export type AvailableTokenSymbols = "eth" | "dai" | "cdai" | "usdc" | "usdt" | "wbtc" | "xdai" | "matic" | "avax" | "bnb";

export type Address = {
    address: string;
};

export type Instances = {
    [chainId in AvailableChainIds]: ChainInstance;
};

export type ChainInstance = {
    [token in AvailableTokenSymbols]?: TokenInstance;
};

export type TokenInstance = {
    instanceAddress: InstanceAddress;
    tokenAddress?: string;
    symbol: AvailableTokenSymbols;
    decimals: number;
};

export type InstanceAddress = {
    "0.1"?: string;
    "1"?: string;
    "10"?: string;
    "100"?: string;
    "500"?: string;
    "1000"?: string;
    "5000"?: string;
    "10000"?: string;
    "50000"?: string;
    "100000"?: string;
    "500000"?: string;
    "5000000"?: string;
};

export type Mining = Address & {
    initialBalance: string;
    rates: Rate[];
};

export type Rate = {
    instance: string;
    value: string;
};

export type RewardSwap = Address & {
    poolWeight: number;
};

export type Torn = Address & {
    capitalization: string;
    pausePeriod: number;
    distribution: { [key: string]: Distribution };
};

export type Distribution = {
    to: string;
    amount: string;
};

export type TornadoTrees = Address & {
    levels: number;
};

export interface Vesting {
    team1: Governance;
    team2: Governance;
    team3: Governance;
    team4: Governance;
    team5: Governance;
    governance: Governance;
}

export type Governance = Address & {
    cliff: number;
    duration: number;
    beneficiary?: string;
};

export type Voucher = Address & {
    duration: number;
};
